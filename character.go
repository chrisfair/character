package character

import (
	"github.com/justinian/dice"
	"math/rand"
	"time"
)

var specifiedStatMethod string = "3d6"
var definedSeed int64 = 0
var seedFromTime = true

type Properties struct {
	hitPointBonus   int
	armorClassBonus int
	movementBonus   int
	toHitBonus      int
	damageBonus     int
	convinceBonus   int
}

type Stat struct {
	properties []Properties
	value      int
}

type Class struct {
	description string
	properties  []Properties
}

type Character struct {
	stats map[string]Stat
	level int
	class string
	race  string
}

func newCharacter() (character Character) {
	character.stats = make(map[string]Stat)
	return
}

func SpecifyStatMethod(statMethod string) (err error) {
	specifiedStatMethod = statMethod
	_, _, err = dice.Roll(specifiedStatMethod)
	return
}

func rollStat(statDesignator string) (result int, err error) {

	setSeed()
	interimResult, _, err := dice.Roll(specifiedStatMethod)
	if err == nil {
		result = interimResult.Int()
	} else {
		result = -1
	}
	return
}

func setSeed() {
	currentSeed := getSeed()
	rand.Seed(int64(currentSeed))
}

func getSeed() (seed int64) {
	if seedFromTime {
		seed = int64(time.Now().UnixNano())
	} else {
		seed = definedSeed
	}
	return
}

func setSeedFromTime(fromTime bool) {
	seedFromTime = fromTime
}

func defineSeed(seed int64) {
	definedSeed = seed
}

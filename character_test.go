package character

import "testing"

func TestSpecifyStatMethod(t *testing.T) {
	validMethod := SpecifyStatMethod("3d6")

	if validMethod != nil {
		t.Errorf("3d6 should be valid!")
	}

	invalidMethod := SpecifyStatMethod("3xb3e")
	if invalidMethod == nil {
		t.Errorf("3xb3e should be an invalid string but it is okay!")
	}

	validMethod = SpecifyStatMethod("3d6")
	if invalidMethod == nil {
		t.Errorf("3d6 should be valid again!")
	}

}

func TestRollStat(t *testing.T) {
	setSeedFromTime(false)
	defineSeed(1)
	standardCheck, err := rollStat("strength")
	if err != nil {
		t.Errorf("This error occured %s", err.Error())
	} else {
		if (standardCheck < 2) || (standardCheck > 19) {
			t.Errorf("This should not be between 2 and 19 but got %d", standardCheck)
		}

		if standardCheck != 16 {
			t.Errorf("This should be not be %d but instead 16", standardCheck)
		}
		setSeedFromTime(true)
		standardCheck, _ := rollStat("strength")
		if (standardCheck < 2) || (standardCheck > 19) {
			t.Errorf("This should not be between 2 and 19 but got %d", standardCheck)
		}
		specifiedStatMethod = "lkjlkjlkj"
		standardCheck, err = rollStat("strength")

		if err == nil {
			t.Errorf("This should result in an error but did not")
		} else {

			if standardCheck != -1 {
				t.Errorf("This should be -1 not %d", standardCheck)
			}

		}

	}
}
